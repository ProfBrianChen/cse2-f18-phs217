/* 

Philip Stefanov
Professor Kalafut
CSE 002
9/5/2018

My Cyclometer - This program will print information about 2 bicycle trips. 
Specifically, the program will print the number of minutes for each trip, the number of counts for each trip, the distance of each trip in miles,
and the distance of the 2 trips combined in miles to the main terminal.

*/

public class Cyclometer {
  
  // main method required for every Java program
  
  public static void main(String[] args){
    
    // input data (time variables)

    int secsTrip1 = 480; // number of seconds the first trip takes
    int secsTrip2 = 3220; // number of seconds the second trip takes
    int countsTrip1 = 1561; // number of wheel rotations that occur in the first trip
    int countsTrip2 = 9037; // number of wheel rotations that occur in the second trip
    
    // input data (distance variables)
    
    double wheelDiameter = 27.0,  // the diameter of the bicycle wheel
    PI = 3.14159, // the value of pi
    feetPerMile = 5280, // number of feet in a mile for conversion between them
    inchesPerFoot = 12, // number of inches in a foot for conversion between them
    secondsPerMinute = 60; // number of seconds in a minute for conversion between them
    double distanceTrip1, distanceTrip2, totalDistance; // distanceTrip1 is the total distance traveled in the first trip; distanceTrip2 is the total discance traveled in the second trip; totalDistance is the total distance traveled in both trips
    
 System.out.println("Trip 1 took "+ (secsTrip1/secondsPerMinute)+" minutes and had "+countsTrip1+" counts." ); // prints a statement of time and number of rotations for trip 1
 System.out.println("Trip 1 took "+ (secsTrip2/secondsPerMinute)+" minutes and had "+countsTrip2+" counts." ); // prints a statement of time and number of rotations for trip 2
    
   // Calculates the total distances traveled in trip 1 and trip 2
    
    distanceTrip1 = countsTrip1 * wheelDiameter * PI; // gives distance in inchesPerFoot
    // for each count, a rotation of the wheel travels the diameter in inches times PI
    distanceTrip1 = inchesPerFoot * feetPerMile; // gives distance for trip 1 in miles
    distanceTrip2 = countsTrip2 * wheelDiameter * PI / inchesPerFoot / feetPerMile; // gives distance for trip 2 in miles
    totalDistance = distanceTrip1 + distanceTrip2; // gives the total distance for both trips
    
    
    // Print the output data
    
System.out.println("Trip 1 was "+distanceTrip1+" miles");
System.out.println("Trip 2 was "+distanceTrip2+" miles");
System.out.println("The total distance was "+totalDistance+" miles");

    
  } // end of main method
} // end of class




     

    
  