/* 

Philip Stefanov
Professor Kalafut
CSE 002
9/11/2018




*/

import java.util.Scanner;

public class Check {
  
  // main method required for every Java program
  
  public static void main(String[] args){
    
 
    Scanner myScanner = new Scanner (System.in);
    
    System.out.print("Enter the original cost of the check in the form xx.xx: "); // prints prompt statement for check cost
    double checkCost = myScanner.nextDouble(); // allows user to enter input for check cost
    
    System.out.print("Enter the percentage tip that you wish to pay as a whole number in the form xx: "); // prints prompt statement for percentage tip
    double tipPercent = myScanner.nextDouble(); // allows user to enter input for percentage tip
    tipPercent /= 100; // converts percentage to a decimal value
    
    System.out.print("Enter the number of people who went to dinner: ");
    int numPeople = myScanner.nextInt();
    
    double totalCost;
    double costPerPerson;
    int dollars, // whole dollar amount of cost
    dimes, pennies; // for storing digits to the right of the decimal place for cost
    totalCost = checkCost * (1 + tipPercent);
    costPerPerson = totalCost / numPeople; // gets the whole amount, dropping decimal fraction
    dollars = (int) costPerPerson; // gets dimes amount, eg.
    // (int)(6.73 * 10) % 10 -> 67 % 10 -> 7
    //  where the % (mod) operator returns the remainder
    //  after the division:   583%100 -> 83, 27%5 -> 2 
    dimes=(int)(costPerPerson * 10) % 10;
    pennies=(int)(costPerPerson * 100) % 10;
    System.out.println("Each person in the group owes $"+ dollars + '.' + dimes + pennies);
    
    
  } // end of main method
} // end of class

