/* 

Philip Stefanov
Professor Kalafut
CSE 002
9/18/2018

Program prompts the user for doubles that represent the square sides
and height of a pyramid, and the program returns the volume of the pyramid with
an explanatory statement

*/

import java.util.Scanner;

public class Pyramid {
  
  // main method required for every Java program
  
  public static void main(String[] args){
    
    double squareSide; // length of square side of pyramid
    double height; // height of pyramid
    
    Scanner myScanner = new Scanner (System.in);
    
    System.out.println("The square side of the pyramid is (input length): "); // prints prompt statement for square side of pyramid
    squareSide = myScanner.nextDouble(); // allows user to enter input for square side of pyramid
    
    System.out.println("The height of the pyramid is (input height): "); // prints prompt statement for height of pyramid
    height = myScanner.nextDouble(); // allows user to enter input for height of pyramid
    
    System.out.print("The volume inside the pyramid is: "+Math.pow(squareSide,2) * (height/3)); /* calculates the volume of the pyramid 
                                                                                                based on the values entered for squareSide and height */
    

    
  } // end of main method
} // end of class

