/*
Philip Stefanov
Professor Kalafut
CSE 002
10/10/2018

The purpose of this program is to print out a "twist" pattern 
based on a length that the user inputs. The program checks if
the input is positive and an Integer, and then generates the 
pattern based on the given length


*/ 
import java.util.Scanner;

public class TwistGenerator {
  public static void main (String[]arg) { //  java main method
    
    Scanner scan = new Scanner(System.in); 
    String junk;
    int length = 0; 
    
    System.out.println("Enter length of twist (Type positive 'Int' needed): ");
    
    while (!scan.hasNextInt()) { // if type 'Int' is not entered, the loop iterates infinitely until it is
      
      junk = scan.next();
      System.out.println("Enter Type 'Int': ");
      
      
    }
    
    length = scan.nextInt(); // user input is assigned to int length
    
    while (length <= 0) { // while the user input is not positive, the loop infinitely iterates until user enters a positive integer
      
      System.out.println("Enter positive type 'Int': ");
      length = scan.nextInt();
      
    }
    
    int count = length; // counter for line 1 of the pattern
    int count2 = length; // counter for line 2 of the pattern
    int count3 = length; // counter for line 3 of the pattern
    
    while (count > 0) { // prints a pattern of 3 characters that repeat in the pattern for the 2st line
      
      if (count > 0) { 
        
        System.out.print("\\");
        count--; // count is decremented after each character to eliminate this specific character in the number of character's long the pattern should be specified by int length
       
       } else {
    
          break;
    
       }
        
      if (count > 0) {
          
        System.out.print(" ");
        count--;
        
      } else {
        
        break;
        
      }
      
      if (count > 0) {
        
        System.out.print("/");
        count--;
        
      } else {
        
        break;
        
      }
          
    }

     System.out.println(""); // starts a new line for the pattern

    while (count2 > 0) { // prints a pattern of 3 characters that repeat in the pattern for the 2nd line
    
      if (count2 > 0) {
        
        System.out.print(" ");
        count2--;
        
      } else {
        
        break;
        
      }
      
      if (count2 > 0) {
        
        System.out.print("X");
        count2--;
        
      } else {
        
        break;
        
      }
      
      if (count2 > 0) {
        
        System.out.print(" ");
        count2--;
        
      } else {
        
        break;
        
      }
      
    }
    
    System.out.println("");
    
    while (count3 > 0) { // prints a pattern of 3 characters that repeat in the pattern for the 3rd line
      
      if (count3 > 0) {
        
        System.out.print("/");
        count3--;
      
      } else {
        
        break;
        
      }
      
      if (count3 > 0) {
        
        System.out.print(" ");
        count3--;
          
      } else {
        
        break;
        
      }
      
      if (count3 > 0) {
        
        System.out.print("\\");
        count3--;
      
       } else {
          
         break;
         
       }
     }
    
    System.out.println("");
    
  }
}