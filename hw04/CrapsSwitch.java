/* 

Philip Stefanov
Professor Kalafut
CSE 002
9/18/2018

The program prompts the user whether they want to randomize dice values 
for 2 dicse or to enter the values for both dice, and then a statement prints
stating the corresponding craps slang for the result of the dice roll

*/

import java.util.Scanner;
import java.util.Random;

public class CrapsSwitch {
  
  // main method required for every Java program
  
  public static void main(String[] args){
    
  
    int d1 = 1; // the first die
    int d2 = 1; //  the second die
    int q = 0; // user answer to 1st question
    Random randGen = new Random(); // converts ramdom number to an integer
    d1 = randGen.nextInt(6) + 1; // generates a random number from 1 to 6 for d1
    d2 = randGen.nextInt(6) + 1; // generates a random number from 1 to 6 for d2
    
    
    Scanner myScanner = new Scanner (System.in);
    System.out.println("Would you like to have dice randomly cast (enter '1') or set values for both dice (enter '2')?");
    q = myScanner.nextInt();
    
    switch (q) {
      case 1: 
      d1 = d1;
      d2 = d2;
      switch (d1) {
        case 1:
        switch (d2) {
          case 1: System.out.println("Snake Eyes");
            break;
          case 2: System.out.println("Ace Deuce");
            break;
          case 3: System.out.println("Easy Four");
            break;
          case 4: System.out.println("Fever Five");
            break;
          case 5: System.out.println("Easy Six");
            break;
          case 6: System.out.println("Seven Out");
            break;
          default: System.out.println("Error");
        }
          break;
        case 2:
        switch (d2){
          case 1: System.out.println("Ace Deuce");
            break;
          case 2: System.out.println("Hard Four");
            break;
          case 3: System.out.println("Fever Five");
            break;
          case 4: System.out.println("Easy Six");
            break;
          case 5: System.out.println("Seven Out");
            break;
          case 6: System.out.println("Easy Eight");
            break;
          default: System.out.println("Error");
        }
          break;
        case 3:
        switch (d2) {
          case 1: System.out.println("Easy Four");
            break;
          case 2: System.out.println("Fever Five");
            break;
          case 3: System.out.println("Hard Six");
            break;
          case 4: System.out.println("Seven Out");
            break;
          case 5: System.out.println("Easy Eight");
            break;
          case 6: System.out.println("Nine");
            break;
          default: System.out.println("Error");
        }
          break;
        case 4:
        switch (d2) {
          case 1: System.out.println("Fever Five");
            break;
          case 2: System.out.println("Easy Six");
            break;
          case 3: System.out.println("Seven Out");
            break;
          case 4: System.out.println("Hard Eight");
            break;
          case 5: System.out.println("Nine");
            break;
          case 6: System.out.println("Easy Ten");
            break;
          default: System.out.println("Error");
        }
          break;
        case 5:
        switch (d2) {
          case 1: System.out.println("Easy Six");
            break;
          case 2: System.out.println("Seven Out");
            break;
          case 3: System.out.println("Easy Eight");
            break;
          case 4: System.out.println("Nine");
            break;
          case 5: System.out.println("Hard Ten");
            break;
          case 6: System.out.println("Yo-leven");
            break;
          default: System.out.println("Error");
        }
          break;
        case 6:
        switch (d2) {
          case 1: System.out.println("Seven Out");
            break;
          case 2: System.out.println("Easy Eight");
            break;
          case 3: System.out.println("Nine");
            break;
          case 4: System.out.println("Easy Ten");
            break;
          case 5: System.out.println("Yo-leven");
            break;
          case 6: System.out.println("Boxcars");
            break;
          default: System.out.println("Error");
        }
          break;
          
      }
        break;
      case 2: 
      System.out.println("Enter value for d1: ");
      d1 = myScanner.nextInt();
      System.out.println("Enter value for d2: ");
      d2 = myScanner.nextInt();
            switch (d1) {
        case 1:
        switch (d2) {
          case 1: System.out.println("Snake Eyes");
            break;
          case 2: System.out.println("Ace Deuce");
            break;
          case 3: System.out.println("Easy Four");
            break;
          case 4: System.out.println("Fever Five");
            break;
          case 5: System.out.println("Easy Six");
            break;
          case 6: System.out.println("Seven Out");
            break;
          default: System.out.println("Error");
        }
          break;
        case 2:
        switch (d2){
          case 1: System.out.println("Ace Deuce");
            break;
          case 2: System.out.println("Hard Four");
            break;
          case 3: System.out.println("Fever Five");
            break;
          case 4: System.out.println("Easy Six");
            break;
          case 5: System.out.println("Seven Out");
            break;
          case 6: System.out.println("Easy Eight");
            break;
          default: System.out.println("Error");
        }
          break;
        case 3:
        switch (d2) {
          case 1: System.out.println("Easy Four");
            break;
          case 2: System.out.println("Fever Five");
            break;
          case 3: System.out.println("Hard Six");
            break;
          case 4: System.out.println("Seven Out");
            break;
          case 5: System.out.println("Easy Eight");
            break;
          case 6: System.out.println("Nine");
            break;
          default: System.out.println("Error");
        }
          break;
        case 4:
        switch (d2) {
          case 1: System.out.println("Fever Five");
            break;
          case 2: System.out.println("Easy Six");
            break;
          case 3: System.out.println("Seven Out");
            break;
          case 4: System.out.println("Hard Eight");
            break;
          case 5: System.out.println("Nine");
            break;
          case 6: System.out.println("Easy Ten");
            break;
          default: System.out.println("Error");
        }
          break;
        case 5:
        switch (d2) {
          case 1: System.out.println("Easy Six");
            break;
          case 2: System.out.println("Seven Out");
            break;
          case 3: System.out.println("Easy Eight");
            break;
          case 4: System.out.println("Nine");
            break;
          case 5: System.out.println("Hard Ten");
            break;
          case 6: System.out.println("Yo-leven");
            break;
          default: System.out.println("Error");
        }
          break;
        case 6:
        switch (d2) {
          case 1: System.out.println("Seven Out");
            break;
          case 2: System.out.println("Easy Eight");
            break;
          case 3: System.out.println("Nine");
            break;
          case 4: System.out.println("Easy Ten");
            break;
          case 5: System.out.println("Yo-leven");
            break;
          case 6: System.out.println("Boxcars");
            break;
          default: System.out.println("Error");
        }
          break;
          
      }
        break;
      default: System.out.println("Incorrect Input Entered");
    }
    
    
    
   
    
    
    
    
    
    
    
  } // end of main method
} // end of class