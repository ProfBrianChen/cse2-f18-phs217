/* 

Philip Stefanov
Professor Kalafut
CSE 002
9/18/2018

The program prompts the user whether they want to randomize dice values 
for 2 dicse or to enter the values for both dice, and then a statement prints
stating the corresponding craps slang for the result of the dice roll

*/

import java.util.Scanner;
import java.util.Random;

public class CrapsIf {
  
  // main method required for every Java program
  
  public static void main(String[] args){
    
  
    int d1 = 1; // the first die
    int d2 = 1; //  the second die
    int q = 0; // user answer to 1st question
    Random randGen = new Random(); // converts ramdom number to an integer
    d1 = randGen.nextInt(6) + 1; // generates a random number from 1 to 6 for d1
    d2 = randGen.nextInt(6) + 1; // generates a random number from 1 to 6 for d2
    
    
    Scanner myScanner = new Scanner (System.in);
    System.out.println("Would you like to have dice randomly cast (enter '1') or set values for both dice (enter '2')?");
    q = myScanner.nextInt();
    
    if (q == 1) {
      
      d1 = d1;
      d2 = d2;
      
      if (d1 == 1 && d2 == 1) {
        
        System.out.println("Snake Eyes");
          
      } if ((d1 == 1 && d2 == 2) || (d1 == 2 && d2 == 1)) {
        
        System.out.println("Ace Deuce");
          
      } if ((d1 == 1 && d2 == 3) || (d1 == 3 && d2 == 1)) {
        
        System.out.println("Easy Four");
          
      } if ((d1 == 1 && d2 == 4) || (d1 == 4 && d2 == 1) || (d1 == 2 && d2 == 3) || (d1 == 3 && d2 == 2)) {
        
        System.out.println("Fever Five");
          
      } if ((d1 == 1 && d2 == 5) || (d1 == 5 && d2 == 1) || (d1 == 2 && d2 == 4) || (d1 == 4 && d2 == 2)) {
        
        System.out.println("Easy Six");
          
      } if ((d1 == 1 && d2 == 6) || (d1 == 6 && d2 == 1) || (d1 == 4 && d2 == 3) || (d1 == 3 && d2 == 4) || (d1 == 2 && d2 == 5) || (d1 == 5 && d2 == 2)) {
        
        System.out.println("Seven Out");
          
      } if ((d1 == 2 && d2 == 6) || (d1 == 6 && d2 == 2) || (d1 == 3 && d2 == 5) || (d1 == 5 && d2 == 3)) {
        
        System.out.println("Easy Eight");
          
      } if ((d1 == 3 && d2 == 6) || (d1 == 6 && d2 == 3) || (d1 == 4 && d2 == 5) || (d1 == 5 && d2 == 4)) {
        
        System.out.println("Nine");
          
      } if ((d1 == 4 && d2 == 6) || (d1 == 6 && d2 == 4)) {
        
        System.out.println("Easy Ten");
          
      } if (d1 == 2 && d2 == 2) {
        
        System.out.println("Hard Four");
          
      } if (d1 == 3 && d2 == 3) {
        
        System.out.println("Hard Six");
          
      } if (d1 == 4 && d2 == 4) {
        
        System.out.println("Hard Eight");
          
      } if (d1 == 5 && d2 == 5) {
        
        System.out.println("Hard Ten");
          
      } if ((d1 == 5 && d2 == 6) || (d1 == 6 && d2 == 4)) {
        
        System.out.println("Yo-leven");
          
      } if (d1 == 6 && d2 == 6) {
        
        System.out.println("Boxcars");
          
      }
      
    } else if (q == 2) {
     
      System.out.println("Enter value for d1: ");
      d1 = myScanner.nextInt();
      System.out.println("Enter value for d2: ");
      d2 = myScanner.nextInt();
      
      if (d1 == 1 && d2 == 1) {
        
        System.out.println("Snake Eyes");
          
      } if ((d1 == 1 && d2 == 2) || (d1 == 2 && d2 == 1)) {
        
        System.out.println("Ace Deuce");
          
      } if ((d1 == 1 && d2 == 3) || (d1 == 3 && d2 == 1)) {
        
        System.out.println("Easy Four");
          
      } if ((d1 == 1 && d2 == 4) || (d1 == 4 && d2 == 1) || (d1 == 2 && d2 == 3) || (d1 == 3 && d2 == 2)) {
        
        System.out.println("Fever Five");
          
      } if ((d1 == 1 && d2 == 5) || (d1 == 5 && d2 == 1) || (d1 == 2 && d2 == 4) || (d1 == 4 && d2 == 2)) {
        
        System.out.println("Easy Six");
          
      } if ((d1 == 1 && d2 == 6) || (d1 == 6 && d2 == 1) || (d1 == 4 && d2 == 3) || (d1 == 3 && d2 == 4) || (d1 == 2 && d2 == 5) || (d1 == 5 && d2 == 2)) {
        
        System.out.println("Seven Out");
          
      } if ((d1 == 2 && d2 == 6) || (d1 == 6 && d2 == 2) || (d1 == 3 && d2 == 5) || (d1 == 5 && d2 == 3)) {
        
        System.out.println("Easy Eight");
          
      } if ((d1 == 3 && d2 == 6) || (d1 == 6 && d2 == 3) || (d1 == 4 && d2 == 5) || (d1 == 5 && d2 == 4)) {
        
        System.out.println("Nine");
          
      } if ((d1 == 4 && d2 == 6) || (d1 == 6 && d2 == 4)) {
        
        System.out.println("Easy Ten");
          
      } if (d1 == 2 && d2 == 2) {
        
        System.out.println("Hard Four");
          
      } if (d1 == 3 && d2 == 3) {
        
        System.out.println("Hard Six");
          
      } if (d1 == 4 && d2 == 4) {
        
        System.out.println("Hard Eight");
          
      } if (d1 == 5 && d2 == 5) {
        
        System.out.println("Hard Ten");
          
      } if ((d1 == 5 && d2 == 6) || (d1 == 6 && d2 == 4)) {
        
        System.out.println("Yo-leven");
          
      } if (d1 == 6 && d2 == 6) {
        
        System.out.println("Boxcars");
          
      }
      
    } else {
      
      System.out.println("Incorrect answer entered");
        
    }
    
    
    
    
   
    
    
    
    
    
    

    
  } // end of main method
} // end of class
