/* 

Philip Stefanov
9/4/2018
CSE 002 
Homework 1: Welcome Class


*/

public class WelcomeClass{
  
  public static void main(String args[]){
    
    /* 
    
  Prints the following to main terminal:
  -----------
  | WELCOME |
  -----------
  ^  ^  ^  ^  ^  ^
 / \/ \/ \/ \/ \/ \
<-E--J--K--0--0--0->
 \ /\ /\ /\ /\ /\ /
  v  v  v  v  v  v
   
   */

    System.out.println("      -----------");
    System.out.println("      | WELCOME |");
    System.out.println("      -----------");
    System.out.println(" ^  ^  ^  ^  ^  ^");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println( "<-P--H--S--2--1--7->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println(" v  v  v  v  v  v");
    System.out.println("My name is Philip Stefanov. I am a CSB major at Lehigh. I play Overwatch and work out during my free time");
    
  }
}