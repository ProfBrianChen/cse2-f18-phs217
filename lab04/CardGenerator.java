/* 

Philip Stefanov
Professor Kalafut
CSE 002
9/18/2018

Program selects a suit and identity of a card at random, 
and prints a statement of the card chosen.

*/

import java.util.Random;

public class CardGenerator {
  
  // main method required for every Java program
  
  public static void main(String[] args){
    
  
    Random randGen = new Random(); // converts ramdom number to an integer
    int randNum = randGen.nextInt(52) + 1; // generates a random number from 1 to 52
    
    
    
    String cardSuit = ""; // suit of the card
    String cardNum = ""; // identity of the card
    
    if ((randNum >= 1) && (randNum <= 13)){ // assigns 1-13 value of randNum to suit diamond
      
      randNum = 52 - randNum;
      cardSuit = "Diamonds";
        
    }
    
    if ((randNum >= 14) && (randNum <= 26)){ // assigns 14-26 value of randNum to suit diamond
      
      randNum = 52 - randNum;
      cardSuit = "Clubs";
        
    }
    
    if ((randNum >= 27) && (randNum <= 39)){ // assigns 27-39 value of randNum to suit diamond
    
      randNum = 52 - randNum;
      cardSuit = "Hearts";
        
    }
    
    if ((randNum >= 40) && (randNum <= 52)){ // assigns 40-52 value of randNum to suit diamond
      
      randNum = 52 - randNum;
      cardSuit = "Spades";
        
    }
    
    switch(randNum){ // assigns special card identity cases 13, 12, 11, and 1 to King, Queen, Jack, and Ace respectively; assigns cards corresponding numbers
        
      case 13: cardNum = "King";
        break;
      case 12: cardNum = "Queen";
        break;
      case 11: cardNum = "Jack";
        break;
      case 2: cardNum = "2";
        break;
      case 3: cardNum = "3";
        break;
      case 4: cardNum = "4";
        break;
      case 5: cardNum = "5";
        break;
      case 6: cardNum = "6";
        break;
      case 7: cardNum = "7";
        break;
      case 8: cardNum = "8";
        break;
      case 9: cardNum = "9";
        break;
      case 10: cardNum = "10";
        break;
      case 1:  cardNum = "Ace";
        break;
      default: System.out.println("unknown");
        break;
    }
    
    System.out.println("You have selected the "+cardNum+" of "+cardSuit); // prints statement of random card chosen
    
    
    
    
    
    
    

    
  } // end of main method
} // end of class

